package br.com.esdras.todentro;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import br.com.esdras.todentro.model.Formula;
import br.com.esdras.todentro.model.Materia;

public class MenuFormulasActivity extends AppCompatActivity {

    ListView listView;
    int materia = 0;
    private static final int REQUEST_CODE = 1, RESULT_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_formulas);

        materia = getIntent().getExtras().getInt("materia");

        listView = findViewById(R.id.listViewFormulas);

        List<Formula> formulasList = FormulaDataSource.obterDadosFormulas().get(materia).getFormulas();
        List<String> formulas = formulasList.stream().map(x -> x.getTitulo()).collect(Collectors.toList());

        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,formulas);

        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), ((TextView)view).getText().toString(),Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), FormulaActivity.class);
                intent.putExtra("indice_formula",i);
                intent.putExtra("materia", materia);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE && resultCode == RESULT_CODE)
        {
            materia = data.getIntExtra("materia", 0);
        }
    }

    public Materia formulaList(int idMateria){
        Materia materia = new Materia();
        switch (idMateria){
            case R.id.matematica:
                materia = FormulaDataSource.obterDadosFormulas().get(0);
                break;
            case R.id.logica:
                materia = FormulaDataSource.obterDadosFormulas().get(1);
            break;
        }

        return materia;
    }
}
