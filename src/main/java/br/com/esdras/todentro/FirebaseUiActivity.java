package br.com.esdras.todentro;

import android.content.Intent;
import android.service.autofill.TextValueSanitizer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Collections;

public class FirebaseUiActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SIGNIN = 100;
    FirebaseAuth mAuth;
    private TextView status, detalhe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_ui);

        mAuth = FirebaseAuth.getInstance();

        status = findViewById(R.id.textViewStatus);
        detalhe = findViewById(R.id.textViewDetalhes);

        findViewById(R.id.buttonLogin).setOnClickListener(this);
        findViewById(R.id.buttonLogout).setOnClickListener(this);

    }

    private void Login(){
        Intent intent = AuthUI.getInstance().createSignInIntentBuilder()
                .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                .setAvailableProviders(Collections.singletonList(new AuthUI.IdpConfig.EmailBuilder().build()))
                .setLogo(R.drawable.atom)
                .build();
        startActivityForResult(intent, SIGNIN);
    }

    private void Logout() {
        AuthUI.getInstance().signOut(this);
        updateUI(null);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGNIN) {
            if (resultCode == RESULT_OK) {
                // Sign in succeeded
//                updateUI(mAuth.getCurrentUser());
                openMenuActivity();
            } else {
                // Sign in failed
                Toast.makeText(this, "Sign In Failed", Toast.LENGTH_SHORT).show();
                updateUI(null);
            }
        }
    }

    private void openMenuActivity(){
        startActivity(new Intent(getApplicationContext(), MenuActivity.class));
        finish();
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            // Signed in
            status.setText(user.getEmail());
            detalhe.setText(user.getUid());

            findViewById(R.id.buttonLogin).setVisibility(View.GONE);
            findViewById(R.id.buttonLogout).setVisibility(View.VISIBLE);
        } else {
            // Signed out
            status.setText("Saiu");
            detalhe.setText(null);

            findViewById(R.id.buttonLogin).setVisibility(View.VISIBLE);
            findViewById(R.id.buttonLogout).setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonLogin:
                Login();
                break;
            case R.id.buttonLogout:
                Logout();
                break;
        }
    }
}
