package br.com.esdras.todentro.model;

import java.util.Objects;

public class Resposta {
    private String resposta;
    private Boolean isCorreta;

    public Resposta() {
    }

    public Resposta(String resposta, Boolean isCorreta) {
        this.resposta = resposta;
        this.isCorreta = isCorreta;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public Boolean getCorreta() {
        return isCorreta;
    }

    public void setCorreta(Boolean correta) {
        isCorreta = correta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Resposta resposta1 = (Resposta) o;
        return Objects.equals(resposta, resposta1.resposta) &&
                Objects.equals(isCorreta, resposta1.isCorreta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resposta, isCorreta);
    }
}
