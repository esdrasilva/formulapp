package br.com.esdras.todentro.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Disciplina {
    private String disciplina;
    private List<Pergunta> perguntas = new ArrayList<>();

    public Disciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public Disciplina setDisciplina(String disciplina) {
        this.disciplina = disciplina;
        return this;
    }

    public List<Pergunta> getPerguntas() {
        return perguntas;
    }

    public Disciplina setPerguntas(List<Pergunta> perguntas) {
        this.perguntas = perguntas;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disciplina that = (Disciplina) o;
        return Objects.equals(disciplina, that.disciplina);
    }

    @Override
    public int hashCode() {
        return Objects.hash(disciplina);
    }
}
