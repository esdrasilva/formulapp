package br.com.esdras.todentro.model;

import java.util.ArrayList;
import java.util.List;

public class Materia {
    private String materia;
    private List<Formula> formulas = new ArrayList<>();

    public Materia(String materia) {
        this.materia = materia;
    }

    public Materia() {
    }

    public String getMateria() {
        return materia;
    }

    public Materia setMateria(String materia) {
        this.materia = materia;
        return this;
    }

    public List<Formula> getFormulas() {
        return formulas;
    }

    public Materia setFormulas(List<Formula> formulas) {
        this.formulas = formulas;
        return this;
    }
}
