package br.com.esdras.todentro.model;

import java.util.ArrayList;
import java.util.List;

public class Acertos {
    private List<String> perguntas = new ArrayList<>();
    private List<String> suaResposta = new ArrayList<>();
    private List<String> respostaCorreta = new ArrayList<>();

    public List<String> getPerguntas() {
        return perguntas;
    }

    public Acertos setPerguntas(List<String> perguntas) {
        this.perguntas = perguntas;
        return this;
    }

    public List<String> getSuaResposta() {
        return suaResposta;
    }

    public Acertos setSuaResposta(List<String> suaResposta) {
        this.suaResposta = suaResposta;
        return this;
    }

    public List<String> getRespostaCorreta() {
        return respostaCorreta;
    }

    public Acertos setRespostaCorreta(List<String> respostaCorreta) {
        this.respostaCorreta = respostaCorreta;
        return this;
    }
}
