package br.com.esdras.todentro.model;

public class Formula {
    public String titulo;
    public String conteudo;
    public int urlImagem;

    public Formula(String titulo, String conteudo, int urlImagem) {
        this.titulo = titulo;
        this.conteudo = conteudo;
        this.urlImagem = urlImagem;
    }

    public String getTitulo() {
        return titulo;
    }

    public Formula setTitulo(String titulo) {
        this.titulo = titulo;
        return this;
    }

    public String getConteudo() {
        return conteudo;
    }

    public Formula setConteudo(String conteudo) {
        this.conteudo = conteudo;
        return this;
    }

    public int getUrlImagem() {
        return urlImagem;
    }

    public Formula setUrlImagem(int urlImagem) {
        this.urlImagem = urlImagem;
        return this;
    }
}
