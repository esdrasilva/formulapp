package br.com.esdras.todentro.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Pergunta {
    private String pergunta;
    private List<Resposta> respostas = new ArrayList<>();
    private Set<Resposta> respostasUsuario = new HashSet<>();

    public Pergunta(String pergunta) {
        this.pergunta = pergunta;
    }

    public String getPergunta() {
        return pergunta;
    }

    public Pergunta setPergunta(String pergunta) {
        this.pergunta = pergunta;
        return this;
    }

    public List<Resposta> getRespostas() {
        return respostas;
    }

    public Pergunta setRespostas(List<Resposta> respostas) {
        this.respostas = respostas;
        return this;
    }

    public Set<Resposta> getRespostasUsuario() {
        return respostasUsuario;
    }

    public Pergunta setRespostasUsuario(Set<Resposta> respostasUsuario) {
        this.respostasUsuario = respostasUsuario;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pergunta)) return false;
        Pergunta pergunta1 = (Pergunta) o;
        return Objects.equals(pergunta, pergunta1.pergunta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pergunta);
    }
}
