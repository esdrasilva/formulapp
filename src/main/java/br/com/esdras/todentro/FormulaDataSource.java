package br.com.esdras.todentro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import br.com.esdras.todentro.model.Formula;
import br.com.esdras.todentro.model.Materia;


public class FormulaDataSource {
    public static List<Materia> obterDadosFormulas (){
        List<Materia> materias = new ArrayList<>();

        final String lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tristique augue ultrices risus dapibus, in porttitor dolor accumsan. Etiam rhoncus neque vitae nisl sagittis consequat";

        Materia programacao = new Materia("Programação");
        programacao.getFormulas().addAll(
                Arrays.asList(
                        new  Formula("Buble Sort", lorem, R.drawable.atom),
                        new  Formula("Insert Sort", lorem, R.drawable.calculator),
                        new  Formula("Linked List", lorem, R.drawable.earth_grid),
                        new  Formula("MVC Pattern", lorem, R.drawable.flask)
                )
        );

        Materia matematica = new Materia("Matemática");
        matematica.getFormulas().addAll(
                Arrays.asList(
                        new  Formula("Baskara", lorem, R.drawable.atom),
                        new  Formula("Pitagoras", lorem, R.drawable.calculator),
                        new  Formula("Área Cone", lorem, R.drawable.earth_grid),
                        new  Formula("Cosena", lorem ,R.drawable.flask)
                )
        );

        materias.addAll(Arrays.asList(programacao, matematica));

        return materias;
    }
}
