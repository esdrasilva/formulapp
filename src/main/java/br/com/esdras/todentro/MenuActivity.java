package br.com.esdras.todentro;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

public class MenuActivity extends AppCompatActivity {

    int materia = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void escolherAtividade(View view) {
        materia = view.getId();
        switch (materia) {
            case R.id.logica:
                exibriMensagem(getString(R.string.programacao), 0);
                break;
            case R.id.matematica:
                exibriMensagem(getString(R.string.matematica), 1);
                break;
            case R.id.quimica:
                exibriMensagem(getString(R.string.quimica), 2);
                break;
            case R.id.fisica:
                exibriMensagem(getString(R.string.fisica), 3);
                break;
        }
    }

    public void exibriMensagem(final String materia, final int posicao) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this)
                .setTitle("Mesnagem")
                .setMessage(String.format("Você selecionou a matéria %s!", materia))
                .setPositiveButton("Abrir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent;
                        if (posicao >= 2) {
                            intent = new Intent(getApplicationContext(), QuestaoActivity.class);
                        } else {
                            intent = new Intent(getApplicationContext(), MenuFormulasActivity.class);
                        }
                        intent.putExtra("materia", posicao);
                        startActivity(intent);
                    }
                });
        alert.create().show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_logout) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
