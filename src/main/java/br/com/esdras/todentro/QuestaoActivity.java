package br.com.esdras.todentro;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.esdras.todentro.model.Disciplina;
import br.com.esdras.todentro.model.Resposta;

public class QuestaoActivity extends AppCompatActivity {
    List<Disciplina> disciplinas;
    Disciplina disciplina;
    int indice_disciplina = 0, indice = 0, acertos = 0;
    TextView materia;
    TextView pergunta;
    RadioButton alternativaA;
    RadioButton alternativaB;
    RadioButton alternativaC;
    RadioButton alternativaD;
    RadioGroup radioGroup;
    Map<String, String> respostas = new HashMap<>();
    int soma = 0;
    int[] pontos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questao);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        materia = findViewById(R.id.textViewMateria);
        pergunta = findViewById(R.id.textViewPergunta);
        alternativaA = findViewById(R.id.radioButtonAlternativaA);
        alternativaA.setChecked(true);
        alternativaB = findViewById(R.id.radioButtonAlternativaB);
        alternativaC = findViewById(R.id.radioButtonAlternativaC);
        alternativaD = findViewById(R.id.radioButtonAlternativaD);
        radioGroup = findViewById(R.id.radioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.radioButtonAlternativaA:
                        isCorreta(disciplina.getPerguntas().get(indice_disciplina).getRespostas().get(0));
                        break;
                    case R.id.radioButtonAlternativaB:
                        isCorreta(disciplina.getPerguntas().get(indice_disciplina).getRespostas().get(1));
                        break;
                    case R.id.radioButtonAlternativaC:
                        isCorreta(disciplina.getPerguntas().get(indice_disciplina).getRespostas().get(2));
                        break;
                    case R.id.radioButtonAlternativaD:
                        isCorreta(disciplina.getPerguntas().get(indice_disciplina).getRespostas().get(3));
                        break;
                }
            }
        });

        disciplinas = DataSource.obterDadosQuestionario();

        indice = getIntent().getExtras().getInt("materia");

        preencheDisciplina(indice);
        pontos = new int[disciplina.getPerguntas().size()];

        atualizaPerguntas();


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proximaQuestao();
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                proximaQuestao();
//                            }
//                        }).show();
            }
        });
    }

    private void atualizaPerguntas() {
        materia.setText(disciplina.getDisciplina());
        pergunta.setText(disciplina.getPerguntas().get(indice_disciplina).getPergunta());
        alternativaA.setText(disciplina.getPerguntas().get(indice_disciplina).getRespostas().get(0).getResposta());
        alternativaB.setText(disciplina.getPerguntas().get(indice_disciplina).getRespostas().get(1).getResposta());
        alternativaC.setText(disciplina.getPerguntas().get(indice_disciplina).getRespostas().get(2).getResposta());
        alternativaD.setText(disciplina.getPerguntas().get(indice_disciplina).getRespostas().get(3).getResposta());
    }

    private void proximaQuestao() {
        if (indice_disciplina < disciplina.getPerguntas().size() - 1) {
            indice_disciplina++;
            atualizaPerguntas();
        } else {
            for (int i = 0; i < pontos.length; i++) {
                soma += pontos[i];
            }
            toast("Ja acabou! Você acertou " + soma + " perguntas!");
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("Resultado")
                    .setMessage("Você acertou " + soma + " perguntas!")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            indice_disciplina = 0;
                            soma = 0;
                            finish();
                        }
                    });
            builder.create().show();
        }
    }

    private void toast(String mensagem) {
        Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT).show();
    }

    private void isCorreta(Resposta resposta) {
        Resposta respostaUsuario = new Resposta(resposta.getResposta(), resposta.getCorreta());
        disciplina.getPerguntas().get(indice_disciplina).getRespostasUsuario().add(respostaUsuario);

        if (resposta.getCorreta()) {
            toast("Resposta Correta! " + resposta.getResposta());
            pontos[indice_disciplina] = 1;
        } else {
            pontos[indice_disciplina] = 0;
        }
    }

    private void preencheDisciplina(int indice) {
        switch (indice) {
            case 2:
                disciplina = disciplinas.get(0);
                break;
            case 3:
                disciplina = disciplinas.get(1);
                break;
        }
    }
}
