package br.com.esdras.todentro;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import br.com.esdras.todentro.model.Formula;
import br.com.esdras.todentro.viewholder.FormulaViewHolder;

public class MathematicActivity extends AppCompatActivity {

    FirebaseDatabase firebaseDatabase;
    FirebaseRecyclerAdapter firebaseRecyclerAdapter;
    FirebaseRecyclerOptions<Formula> firebaseRecyclerOptions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mathematic);

        firebaseDatabase = FirebaseDatabase.getInstance();
        Query query = firebaseDatabase.getReference()
                .child("matematica");

        firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<Formula>()
                .setQuery(query, Formula.class).build();

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Formula, FormulaViewHolder>(firebaseRecyclerOptions) {
            @Override
            protected void onBindViewHolder(@NonNull FormulaViewHolder holder, int position, @NonNull Formula model) {

            }


            @NonNull
            @Override
            public FormulaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                return null;
            }
        };

    }
}
