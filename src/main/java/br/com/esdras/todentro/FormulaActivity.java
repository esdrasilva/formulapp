package br.com.esdras.todentro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.esdras.todentro.model.Formula;
import br.com.esdras.todentro.model.Materia;

public class FormulaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formula);

        TextView titulo = findViewById(R.id.textViewTitulo);
        TextView conteudo = findViewById(R.id.textViewConteudo);
        ImageView imagem = findViewById(R.id.imageViewImagem);

        int indice = getIntent().getExtras().getInt("indice_formula");
        int indice_materia = getIntent().getExtras().getInt("materia");

        Materia materia = FormulaDataSource.obterDadosFormulas().get(indice_materia);

        Formula formula = materia.getFormulas().get(indice);

        titulo.setText(formula.getTitulo());
        conteudo.setText(formula.getConteudo());
        imagem.setImageDrawable(getDrawable(formula.getUrlImagem()));

        Intent intent = new Intent();
        intent.putExtra("materia", indice_materia);

        setResult(2, intent);

    }
}
