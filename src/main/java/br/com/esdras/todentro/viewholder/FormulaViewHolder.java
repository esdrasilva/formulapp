package br.com.esdras.todentro.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.esdras.todentro.R;
import br.com.esdras.todentro.model.Formula;

public class FormulaViewHolder extends RecyclerView.ViewHolder {

    public TextView titulo;
    public TextView conteudo;
    public ImageView imagem;

    public FormulaViewHolder(@NonNull View itemView) {
        super(itemView);
        titulo = itemView.findViewById(R.id.textViewTitulo);
        conteudo = itemView.findViewById(R.id.textViewConteudo);
        imagem = itemView.findViewById(R.id.imageViewImagem);
    }


}
