package br.com.esdras.todentro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import br.com.esdras.todentro.model.Disciplina;
import br.com.esdras.todentro.model.Pergunta;
import br.com.esdras.todentro.model.Resposta;

public class DataSource {

    public static List<Disciplina> obterDadosQuestionario(){

        Disciplina quimica = new Disciplina("Química");

        Pergunta p1 = new Pergunta("Primeira questão de química!?");
        Resposta r1p1 = new Resposta("Primeira opção.", Boolean.FALSE);
        Resposta r2p1 = new Resposta("Segunda opção.", Boolean.FALSE);
        Resposta r3p1 = new Resposta("Terceira opção.", Boolean.TRUE);
        Resposta r4p1 = new Resposta("Quarta opção.", Boolean.FALSE);

        p1.getRespostas().addAll(Arrays.asList(r1p1, r2p1, r3p1, r4p1));

        Pergunta p2 = new Pergunta("Segunda questão de química!");
        Resposta r1p2 = new Resposta("Primeira opção.", Boolean.FALSE);
        Resposta r2p2 = new Resposta("Segunda opção.", Boolean.FALSE);
        Resposta r3p2 = new Resposta("Terceira opção.", Boolean.TRUE);
        Resposta r4p2 = new Resposta("Quarta opção.", Boolean.FALSE);

        p2.getRespostas().addAll(Arrays.asList(r1p2, r2p2, r3p2, r4p2));

        quimica.getPerguntas().addAll(Arrays.asList(p1,p2));


        Disciplina fisica = new Disciplina("Física");

        Pergunta f1 = new Pergunta("O que é cinética?");
        Resposta r1f1 = new Resposta("Primeira opção.", Boolean.FALSE);
        Resposta r2f1 = new Resposta("Segunda opção.", Boolean.FALSE);
        Resposta r3f1 = new Resposta("Terceira opção.", Boolean.TRUE);
        Resposta r4f1 = new Resposta("Quarta opção.", Boolean.FALSE);

        f1.getRespostas().addAll(Arrays.asList(r1f1, r2f1, r3f1, r4f1));

        Pergunta f2 = new Pergunta("O que é eletricidade?");
        Resposta r1f2 = new Resposta("Primeira opção.", Boolean.FALSE);
        Resposta r2f2 = new Resposta("Segunda opção.", Boolean.TRUE);
        Resposta r3f2 = new Resposta("Terceira opção.", Boolean.FALSE);
        Resposta r4f2 = new Resposta("Quarta opção.", Boolean.FALSE);

        f2.getRespostas().addAll(Arrays.asList(r1f2, r2f2, r3f2, r4f2));

        Pergunta f3 = new Pergunta("O que é eletricidade estática?");
        Resposta r1f3 = new Resposta("Primeira opção.", Boolean.FALSE);
        Resposta r2f3 = new Resposta("Segunda opção.", Boolean.TRUE);
        Resposta r3f3 = new Resposta("Terceira opção.", Boolean.FALSE);
        Resposta r4f3 = new Resposta("Quarta opção.", Boolean.FALSE);

        f3.getRespostas().addAll(Arrays.asList(r1f3, r2f3, r3f3, r4f3));

        fisica.getPerguntas().addAll(Arrays.asList(f1,f2,f3));

        List<Disciplina> disciplinas = new ArrayList<>();
        disciplinas.add(quimica);
        disciplinas.add(fisica);
        return disciplinas;
    }

//    public static void main(String[] args) {
//        List<Disciplina> disciplinas = obterDadosQuestionario();
//
//        //Pegamos a materia portugues
//        Disciplina disciplinaPt = obterDadosQuestionario().get(0);
//
//        //Pegamos a primeira pergunta - .get(0)
//        Pergunta pergunta = disciplinaPt.getPerguntas().get(0);
//
//        //Pegamos todas as respostas da pergunta 1
//        List<Resposta> respostasPerguntaUm = pergunta.getRespostas();
//
//        //Pegamos a quarta resposta da pergunta 1
//        Resposta quartaRespostaPerguntaUm = respostasPerguntaUm.get(3);
//
//        System.out.println(pergunta.getPergunta());
//        Disciplina disciplinaFs = obterDadosQuestionario().get(1);
//
//        System.out.println(disciplinas);
//    }
}
